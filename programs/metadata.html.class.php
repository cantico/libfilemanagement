<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/metadata.class.php';



class lfm_HtmlMetadata extends lfm_MetadataNamespace_Doc {

	private $parsed = false;
	

	public function getAllMeta() {
		return array('Title');
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return 	mixed
	 */
	public function getMetaValue($name) {
		if (false === $this->parsed) {
			$this->getAllHtmlMeta();
		}

		return $this->getStoredValue($name);
	}


	/**
	 * parse html file
	 */
	private function getAllHtmlMeta() {

		$this->parsed = true;

		$filepath = $this->getFilePath();
		$buffer = @file($filepath); //Hide warnings.

		if (!is_array($buffer))
			return false;

		$buffer = implode(' ', $buffer);
		$lowBuffer = strtolower($buffer);

		/* Locate where <TITLE> is located in html file. */
		$titlepos = strpos($lowBuffer, '<title>');
		if (false !== $titlepos)
		{
			$lBound = $titlepos + 7; // 7 is the lengh of <TITLE>.
	
			if ($lBound > 7) {
	
				/* Locate where </TITLE> is located in html file. */
				$uBound = strpos($lowBuffer, '</title>', $lBound);
			
				if ($uBound > $lBound) {
				
					$title = substr($buffer, $lBound, $uBound - $lBound);
					$title = trim(html_entity_decode(strip_tags($title)));
				
					if (strlen($title) > 0)  {
						$this->setMeta('Title', $title);
						return true;
					}
				}
			}
		}
		return false;
	}
}




