<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


class lfm_dropShadow
{
	var $_imgOrig;       // the original image
	var $_imgFinal;      // the final image, resized
	var $_imgShadow;     // the final image with a drop shadow, if applied
	var $_shadows;       // the dropshadow image array
	var $_shadowPath;    // the path to the dropshadow images


	function lfm_dropShadow()
	{
		$this->_imgOrig = $this->_imgFinal = $this->_imgFinal = NULL;
		$this->_shadows = array();
	}


	/**
	 * loads the original image.  If no extension is passed to $forceext then it will attempt
	 * to load the correct type based on the $filename.
	 *
	 * @param	string	$filename
	 */
	function loadImage($filename)
	{
		if (!@file_exists($filename))
		{
			bab_debug("The supplied file name '$filename' does not point to an existing file.");
			return FALSE;
		}
		
		$ext = $this->_getExtension($filename);
		if ($ext == 'jpg') $ext = 'jpeg';

		$func = "imagecreatefrom$ext";
		
		if (!@function_exists($func))
		{
			bab_debug("That file cannot be loaded with the function '$func'.");
			return FALSE;
		}
		
		$this->_imgOrig = @$func($filename);
		
		if ($this->_imgOrig == NULL)
		{
			bab_debug('The image could not be loaded.');
			return FALSE;
		}
		
		return TRUE;
	}
	

	/**
	 * saves the final image resource to the file system.
	 * it will attempt to save based on the file name.
	 * $quality is a value from 1 to 100, inclusive, and only used when outputting as a jpg
	 *
	 * @param	string	$filename
	 * @return  boolean
	 */
	function saveShadowedImage($filename)
	{
		return ($this->_saveImage($filename, $this->_imgShadow, '', 80));
	}



	/**
	 * allows you to set where the drop-shadow images will be located
	 * @param	string	$path
	 */
	function setShadowPath($path = '.')
	{
		$this->_shadowPath = realpath($path);
		if ($this->_shadowPath[strlen($this->_shadowPath)-1] != '/') $this->_shadowPath .= '/';
	}

	
	/**
	 * apply the drop shadow to the final image resource
	 * this will overwrite the final image resource with the drop-shadowed version
	 * the background colour can be changed by passing an HTML hex value (with or without the #)
	 *
	 * @param	string	[$bgcolour]
	 */
	function applyShadow($bgcolour = 'FFFFFF')
	{
		// make sure we have the image resource
		if ($this->_imgFinal == NULL)
		{
			//bab_debug('There is no resized image, so the original image is being used.');
			if ($this->_imgOrig == NULL)
			{
				bab_debug('There is no original image loaded.');
				return FALSE;
			}
			$this->_resizeImage(@ImageSX($this->_imgOrig), @ImageSY($this->_imgOrig), 'applyShadow');
			if ($this->_imgFinal == NULL)
			{
				bab_debug('The destination image could not be created.');
				return FALSE;
			}
		}

		// attempt to load the drop shadow array
		if (!isset($this->_shadows['l'])) $this->_shadows['l']  = @ImageCreateFromPNG($this->_shadowPath . "ds_left.png");
		if (!isset($this->_shadows['r'])) $this->_shadows['r']  = @ImageCreateFromPNG($this->_shadowPath . "ds_right.png");
		if (!isset($this->_shadows['t'])) $this->_shadows['t']  = @ImageCreateFromPNG($this->_shadowPath . "ds_top.png");
		if (!isset($this->_shadows['b'])) $this->_shadows['b']  = @ImageCreateFromPNG($this->_shadowPath . "ds_bottom.png");
		if (!isset($this->_shadows['tl'])) $this->_shadows['tl'] = @ImageCreateFromPNG($this->_shadowPath . "ds_tlcorner.png");
		if (!isset($this->_shadows['tr'])) $this->_shadows['tr'] = @ImageCreateFromPNG($this->_shadowPath . "ds_trcorner.png");
		if (!isset($this->_shadows['bl'])) $this->_shadows['bl'] = @ImageCreateFromPNG($this->_shadowPath . "ds_blcorner.png");
		if (!isset($this->_shadows['br'])) $this->_shadows['br'] = @ImageCreateFromPNG($this->_shadowPath . "ds_brcorner.png");

		// verify all is well
		foreach($this->_shadows as $key => $val)
		{
			if ($val == NULL)
			{
				bab_debug('The shadow files could not be loaded.');
				return FALSE;
			}
		}

		// create go-between image

		// original image
		$ox = @ImageSX($this->_imgFinal);
		$oy = @ImageSY($this->_imgFinal);

		// new image with shadow
		$nx = @ImageSX($this->_shadows['l']) + @ImageSX($this->_shadows['r']) + @ImageSX($this->_imgFinal);
		$ny = @ImageSY($this->_shadows['t']) + @ImageSY($this->_shadows['b']) + @ImageSY($this->_imgFinal);


		// left
		$lx = @ImageSX($this->_shadows['l']);
		$ly = @ImageSY($this->_shadows['l']);

		// right
		$rx = @ImageSX($this->_shadows['r']);
		$ry = @ImageSY($this->_shadows['r']);

		// top
		$tx = @ImageSX($this->_shadows['t']);
		$ty = @ImageSY($this->_shadows['t']);
		
		// bottom
		$bx = @ImageSX($this->_shadows['b']);
		$by = @ImageSY($this->_shadows['b']);




		// top left corner
		$tlx = @ImageSX($this->_shadows['tl']);
		$tly = @ImageSY($this->_shadows['tl']);

		// top right corner
		$trx = @ImageSX($this->_shadows['tr']);
		$try = @ImageSY($this->_shadows['tr']);

		// bottom left corner
		$blx = @ImageSX($this->_shadows['bl']);
		$bly = @ImageSY($this->_shadows['bl']);

		// bottom right corner
		$brx = @ImageSX($this->_shadows['br']);
		$bry = @ImageSY($this->_shadows['br']);


		// bab_debug("Original image size = $ox/$oy : Drop shadowed image size = $nx/$ny");

		$this->_imgShadow = @ImageCreateTrueColor($nx, $ny);
		if ($this->_imgShadow == NULL)
		{
			bab_debug('The drop-shadowed image resource could not be created.');
			return FALSE;
		}

		// pre-process the image
		$background = $this->_htmlHexToBinArray($bgcolour);
	 	@ImageAlphaBlending($this->_imgShadow, TRUE);
		@ImageFill($this->_imgShadow, 0, 0, @ImageColorAllocate($this->_imgShadow, $background[0], $background[1], $background[2]));

		// apply the shadow



		



	
		// use a portion of border image if orignal is smaller than the border
		// otherwise extends the border to the size of the original

		$src_tx = $ox < $tx ? $ox : $tx;
		$src_ly = $oy < $ly ? $oy : $ly;
		$src_ry = $oy < $ry ? $oy : $ry;
		$src_bx = $ox < $bx ? $ox : $bx;
		
		// top shadow
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['t'],
						$lx	, 0		, 0			, 0,
						$ox	, $ty	, $src_tx	, $ty
		);
		
		// left shadow
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['l'],
						0	, $ty	, 0			, 0,
						$lx	, $oy	, $lx		, $src_ly
		);



		// right shadow
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['r'],
				($nx - $rx)	, $tly	, 0			, 0,
						$rx	, $oy	, $rx		, $src_ry
		);
		
		// bottom shadow
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['b'],
						$lx, ($ny - $by), 0			, 0,
						$ox, $by		, $src_bx	, $by
		);


		

		
		// top left corner
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['tl'],
						0, 0, 0, 0,
						$tlx, $tly, $tlx, $tly
		);

		// top right corner
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['tr'],
						($nx - $rx), 0, 0, 0,
						$trx, $try, $trx, $try
		);

		// bottom left corner
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['bl'],
						0, ($ny - $by), 0, 0,
						$blx, $bly, $blx, $bly
		);


		// bottom right corner
		@ImageCopyResampled($this->_imgShadow, $this->_shadows['br'],
						($nx - $rx), ($ny - $by), 0, 0,
						$brx, $bry, $brx, $bry
		);
		


		// apply the picture
		@ImageCopyResampled($this->_imgShadow, $this->_imgFinal,
						$lx, $ty, 0, 0,
						$ox, $oy, $ox, $oy
		);


		


		return TRUE;
	}

	/**
	 * clean up image resources
	 * 0 = original image only
	 * 1 = final image only
	 * 2 = shadow images only
	 * 3 = all images
	 */
	function flushImages($which = 5)
	{
		switch ($which)
		{
			case 0:
				@ImageDestroy($this->_imgOrig);
				$this->_imgOrig = NULL;
				break;
			case 1:
				@ImageDestroy($this->_imgFinal);
				$this->_imgFinal = NULL;
				break;
			case 3:
				@ImageDestroy($this->_imgShadow);
				$this->_imgShadow = NULL;
				break;
			case 4:
				@ImageDestroy($this->_shadows['l']);
				@ImageDestroy($this->_shadows['r']);
				@ImageDestroy($this->_shadows['t']);
				@ImageDestroy($this->_shadows['b']);
				@ImageDestroy($this->_shadows['tl']);
				@ImageDestroy($this->_shadows['tr']);
				@ImageDestroy($this->_shadows['bl']);
				@ImageDestroy($this->_shadows['br']);
				unset($this->_shadows);
				break;
			case 5:
				@ImageDestroy($this->_imgOrig);
				@ImageDestroy($this->_imgFinal);
				@ImageDestroy($this->_imgShadow);
				@ImageDestroy($this->_shadows['l']);
				@ImageDestroy($this->_shadows['r']);
				@ImageDestroy($this->_shadows['t']);
				@ImageDestroy($this->_shadows['b']);
				@ImageDestroy($this->_shadows['tl']);
				@ImageDestroy($this->_shadows['tr']);
				@ImageDestroy($this->_shadows['bl']);
				@ImageDestroy($this->_shadows['br']);
				$this->_imgOrig = $this->_imgFinal = $this->_imgShadow = NULL;
				unset($this->_shadows);
				break;
		}
	}


	

	/**
	 * get the file extension of the given filename
	 * @param	string	$filename
	 */
	function _getExtension($filename)
	{
		$ext  = strtolower(substr($filename, (strrpos($filename, '.') ? strrpos($filename, '.') + 1 : strlen($filename)), strlen($filename)));
		if ($ext == 'jpg') $ext = 'jpeg';
		return $ext;
	}
	
	/**
	 * if only the width is supplied, get the height based on the original image size
	 * if only the height is supplied, get the width based on the original image size
	 *
	 * @param	int	$x
	 * @param	int	$y
	 */
	function _getProportionalSize($x, $y)
	{
		if (!$x) $x = ($y / ImageSY($this->_imgOrig)) * ImageSX($this->_imgOrig);
		else $y = ImageSY($this->_imgOrig) / (ImageSX($this->_imgOrig) / $x);
		return array($x, $y);
	}

	/**
	 * core functionality for the resizing of an image
	 */
	function _resizeImage($nx, $ny, $function)
	{
		if ($this->_imgOrig == NULL)
		{
			bab_debug('The original image has not been loaded.');
			return FALSE;
		}
		if (($nx < 0) || ($ny < 0))
		{
			bab_debug('The image could not be resized because the size given is not valid.');
			return FALSE;
		}
		if ($this->_imgFinal) $this->flushImages(1);
		$this->_imgFinal = @ImageCreateTrueColor($nx, $ny);
		@ImageCopyResampled($this->_imgFinal, $this->_imgOrig, 0, 0, 0, 0, $nx, $ny, @ImageSX($this->_imgOrig), @ImageSY($this->_imgOrig));
	}

	/**
	 * show the final image as either a png or jpg
	 * $quality is a value from 1 to 100, inclusive, and only used when outputting as a jpg
	 * @param	ressource	$resource
	 * @param	string		$type
	 * @param	int			$quality
	 */
	function _showImage($resource, $type, $quality)
	{
		if ($resource == NULL)
		{
			bab_debug('There is no processed image to show.');
			return FALSE;
		}
		if ($type == 'png')
		{
			header('Content-Type: image/png');
			echo @ImagePNG($resource);
			return TRUE;
		}
		else if ($type == 'jpg' || $type == 'jpeg')
		{
			header('Content-Type: image/jpeg');
			echo @ImageJPEG($resource, '', $quality);
			return TRUE;
		}
		else
		{
			bab_debug("Could not show the output file as a $type.");
			return FALSE;
		}
	}

	/**
	 * saves the image resource to the file system.  If no extension is given for $type
	 * then it will attempt to save based on the file name.
	 * $quality is a value from 1 to 100, inclusive, and only used when outputting as a jpg
	 * $img is the image resource
	 *
	 * @param	string		$filename
	 * @param	ressource	$img
	 * @param	string		$type
	 * @param	int			$quality
	 *
	 * @return boolean
	 */
	function _saveImage($filename, $img, $type, $quality)
	{
		if ($img == NULL)
		{
			bab_debug('There is no processed image to save.');
			return FALSE;
		}

		$ext = ($type == '' ? $this->_getExtension($filename) : $type);
		if ($ext == 'jpg') $ext = 'jpeg';
		$func = "image$ext";
		
		if (!@function_exists($func))
		{
			bab_debug("That file cannot be saved with the function '$func'.");
			return FALSE;
		}

		if ($ext == 'png') $saved = @$func($img, $filename);
		if ($ext == 'jpeg') $saved = @$func($img, $filename, $quality);
		if ($saved == FALSE)
		{
			bab_debug("Could not save the output file '$filename' as a $ext.");
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	/**
	 * convert HTML hex value into integer array
	 * @param	string	$hex
	 */
	function _htmlHexToBinArray($hex)
	{
		$hex = preg_replace('/^#/', '', $hex);
		
		for ($i=0; $i<3; $i++)
		{
			$foo = substr($hex, 2*$i, 2); 
			$rgb[$i] = 16 * hexdec(substr($foo, 0, 1)) + hexdec(substr($foo, 1, 1)); 
		}
		return $rgb;
	}



}

?>