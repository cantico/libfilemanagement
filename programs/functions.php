<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

function lfm_translate($str) {
	return bab_translate($str, 'LibFileManagement');
}




/**
 * Get directory
 * @param	string	$uid
 * @return string
 */
function lfm_getDirectory($uid) {
	
	if (empty($uid))
	{
		throw new Exception('the uid must not be empty');
	}
	

	$subdir1 = $uid{0};
	if (isset($uid{1})) {
		$subdir2 = $uid{1};
	} else {
		$subdir2 = $uid{0};
	}

	$addon = bab_getAddonInfosInstance('LibFileManagement');
	$directory = $addon->getUploadPath().'storage/'.$subdir1.'/'.$subdir2.'/'.$uid.'/';
	lfm_createDir($directory);
	
	return $directory;
}



function lfm_getFiles($uid) {

	$directory = lfm_getDirectory($uid);
	
	
	$dirfiles = array();
	if (is_dir($directory)){
		if ($dh = opendir($directory)) {
		   while (($file = readdir($dh)) !== false) {
			   if (is_file($directory.$file)) {
					$dirfiles[] = $directory.$file;
				}
		   }
		   closedir($dh);
		}
	}
	
	if (!$dirfiles) {
		return false;
	}
	
	bab_sort::sort($dirfiles);
	return $dirfiles;
}




/**
 * return path to file or false if no file
 * @param	string	$uid
 * @return string|false
 */
function lfm_getFile($uid) {
	$files = lfm_getFiles($uid);
	if (false === $files) {
		return $files;
	}
	return reset($files);
}







/**
 * @param	string	$str	: directory full path
 */
function lfm_createDir($str) {
	
    require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
    
	$p = new bab_Path($str);
	$p->createDir();
	
}



/**
 * Delete uploaded file
 * @param	string	$uid
 */
function lfm_emptyDirectory($uid) {
	while (false !== $current_file = lfm_getFile($uid)) {
		unlink($current_file);
	}
}








/**
 * Freedesktop.org mime descriptions
 */
function lfm_getmime() {

	global $babDB;

	$en = array();
	$fr = array();
	
	$res = $babDB->db_query('SELECT mimetype FROM bab_mime_types');
	while ($arr = $babDB->db_fetch_assoc($res)) {
		$xmlfile = '/usr/share/mime/'.$arr['mimetype'].'.xml';
		
		if (is_readable($xmlfile)) {
			$xml = new SimpleXMLElement(str_replace('<comment xml:lang="','<comment lang="',file_get_contents($xmlfile)));

			
			$en[] = sprintf('<string id="%s">%s</string>', $arr['mimetype'], (string) utf8_decode($xml->comment[0]));
			
			
			
			foreach($xml->comment as $comment) {
				if ('fr' === (string) $comment->attributes()->lang) {
					$fr[] = sprintf('<string id="%s">%s</string>', $arr['mimetype'], utf8_decode((string) $comment));
				}
			}

		} 
	}
	
	
	echo implode("\n", $fr);
	die();
}














/**
 * @param	string	$str
 * @return 	string
 */
function lfm_getStringFromCommandLine($str) {

	if ('windows' === bab_browserOS()) {
		$default_charset = 'ISO-8859-1';
	} else {
		$default_charset = 'UTF-8';
	}

	return bab_getStringAccordingToDataBase($str, $default_charset);
}


/**
 * 
 * @return bool
 */
function lfm_systemAccess()
{
	static $system_access = null;
	
	if (null === $system_access)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibFileManagement/');
		
		$isWindows = (DIRECTORY_SEPARATOR === '\\');
		
		// By default, no system access on windows servers.
		$system_access = $registry->getValue('system_access', !$isWindows);
	}
	
	return $system_access;
}


function lfm_system($cmd, &$retval)
{
	if (!lfm_systemAccess())
	{
		$retval = 1;
		return '';
	}
	
	return system($cmd, $retval);
}

function lfm_execCmd($cmd) {
	
	if (!lfm_systemAccess())
	{
		return false;
	}

	$handle = popen($cmd, 'r');
	if ($handle) {
		$buffer = '';
		while(!feof($handle)) {
		   $buffer .= fgets($handle, 1024);
		}
		pclose($handle);
		return $buffer;
	} 
	
	return false;
}


