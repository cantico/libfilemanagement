<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";








function lfm_getGenericClassName($mime) {


	switch($mime) {
		case 'text/plain':
			return 'mimetypes-text-x-generic';
			
		case 'application/pdf':
			 return'mimetypes-application-pdf';
			 
		case 'application/vnd.oasis.opendocument.text':
		case 'application/vnd.oasis.opendocument.text-template':
		case 'application/vnd.oasis.opendocument.text-master':
		case 'application/msword':
		case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
		case 'application/vnd.sun.xml.writer':
			return 'mimetypes-x-office-document';
			
		case 'application/vnd.oasis.opendocument.presentation':
		case 'application/vnd.oasis.opendocument.presentation-template':
		case 'application/vnd.ms-powerpoint':
		case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
			return 'mimetypes-x-office-presentation';
			
		case 'application/vnd.oasis.opendocument.spreadsheet':
		case 'application/vnd.oasis.opendocument.spreadsheet-template':
		case 'application/vnd.ms-excel':
		case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
			return 'mimetypes-x-office-spreadsheet';
	}
	
	
	if (false !== strpos($mime, 'audio')) {
		return 'mimetypes-audio-x-generic';
	}
	
	if (false !== strpos($mime, 'image')) {
		return 'mimetypes-image-x-generic';
	}
	
	if (false !== strpos($mime, 'video')) {
		return 'mimetypes-video-x-generic';
	}
	
	if (false !== strpos($mime, 'zip') || false !== strpos($mime, 'tar') || false !== strpos($mime, 'rar')) {
		return 'mimetypes-package-x-generic';
	}


	return 'mimetypes-unknown';
}
