<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/metadata.class.php';




class lfm_Id3Metadata extends lfm_MetadataNamespace_Audio {

	private $parsed = false;

	private $ffmpeg = null;


	public function getAllMeta() {

		if (function_exists('id3_get_tag')) {

			$arr = array(
				'Title',
				'Artist',
				'Album',
				'Comment',
				'TrackNo',
				'Genre'
			);

		} elseif(class_exists('ffmpeg_movie')) {

			$arr = array(
				'Title',
				'Artist',
				'Comment',
				'TrackNo',
				'Samplerate',
				'Bitrate'
			);
		}

		return $arr;
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	public function getMetaValue($name) {
		
			
		if (function_exists('id3_get_tag')) {


			if (false === $this->parsed) {
				$this->parsed = true;

				// warning : bug with other id3 versions
				// http://pecl.php.net/bugs/bug.php?id=6990

				$tags = id3_get_tag($this->getFilePath(), ID3_V1_1);

				$this->setMeta('Title'			, $this->getKey($tags, 'title'));
				$this->setMeta('Artist'			, $this->getKey($tags, 'artist'));
				$this->setMeta('Album'			, $this->getKey($tags, 'album'));
				$this->setMeta('Comment'		, $this->getKey($tags, 'comment'));
				$this->setMeta('TrackNo'		, $this->getKey($tags, 'track'));
				$this->setMeta('Genre'			, $this->getKey($tags, 'genre'));

				return $this->getStoredValue($name);
			}
		
		} elseif(class_exists('ffmpeg_movie')) {
			
			if (null === $this->ffmpeg) {
				$this->ffmpeg = new ffmpeg_movie($this->getFilePath(), false);
			}

			switch($name) {
				case 'Title': 		return $this->getFFmpeg('getTitle');
				case 'Artist': 		return $this->getFFmpeg('getArtist');
				case 'Comment': 	return $this->getFFmpeg('getComment');
				case 'TrackNo': 	return $this->getFFmpeg('getTrackNumber');
				case 'Samplerate': 	return $this->getFFmpeg('getAudioSampleRate');
				case 'Bitrate': 	return $this->getFFmpeg('getAudioBitRate');
			}
		}
	}

	
	/**
	 * @param	array	$tags
	 * @return mixed
	 */
	private function getKey($tags, $key) {

		$return = null;

		if (false !== $tags) {
			if (isset($tags[$key])) {
				$return = bab_getStringAccordingToDataBase($tags[$key], 'UTF-8');
			} 
		}

		return $return;
	}

	
	private function getFFmpeg($method) {

		$value = $this->ffmpeg->$method();
		$value = bab_getStringAccordingToDataBase($value, 'UTF-8');

		return $value;
	}
}





