<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/xlsxwriter.class.php';

class lfm_XLSXWriter extends XLSXWriter
{
    protected $sheet_filename = '';
    protected $sheets_close = true;
    protected $file = null;
    protected $toWrite = '';
    protected $lastRow = 0;
    protected $lastCol = 0;
    protected $filename = '';



    public function __construct()
    {
    }


    protected function tempFilename()
    {
        $path = new bab_Path($GLOBALS['babUploadPath'],'tmp',session_id());
        $path->createDir();
        $filename = tempnam($path->tostring(), "xlsx_writer_");
        $this->temp_files[] = $filename;
        return $filename;
    }


    protected function getfile()
    {
        if($this->file === null){
            $this->file = new XLSXWriter_BuffererWriter($this->sheet_filename);
        }
        return $this->file;
    }




    public function addSheet($nbRows, $nbCols, $sheet_name = '')
    {
        if ($this->sheets_close === false) {
            $this->closeSheet();
        }
        $this->sheets_close = false;
        $this->sheet_filename = $sheet_filename = $this->tempFilename();
        $sheet_default = 'Sheet'.(count($this->sheets)+1);
        $sheet_name = !empty($sheet_name) ? $sheet_name : $sheet_default;
        $this->sheets[] = array('filename'=>$sheet_filename, 'sheetname'=>$sheet_name ,'xmlname'=>strtolower($sheet_default).".xml" );

        $max_cell = self::xlsCell( $nbRows-1, $nbCols-1 );

        $tabselected = count($this->sheets)==1 ? 'true' : 'false';//only first sheet is selected

        $file = $this->getfile();

        $file->write(
            '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'."\n".
            '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">'.
                '<sheetPr filterMode="false">'.
                    '<pageSetUpPr fitToPage="false"/>'.
                '</sheetPr>'.
                '<dimension ref="A1:'.$max_cell.'"/>'.
                '<sheetViews>'.
                    '<sheetView colorId="64" defaultGridColor="true" rightToLeft="false" showFormulas="false" showGridLines="true" showOutlineSymbols="true" showRowColHeaders="true" showZeros="true" tabSelected="'.$tabselected.'" topLeftCell="A1" view="normal" windowProtection="false" workbookViewId="0" zoomScale="100" zoomScaleNormal="100" zoomScalePageLayoutView="100">'.
                        '<selection activeCell="A1" activeCellId="0" pane="topLeft" sqref="A1"/>'.
                    '</sheetView>'.
                '</sheetViews>'.
                '<cols>'.
                    '<col collapsed="false" hidden="false" max="1025" min="1" style="0" width="11.5"/>'.
                '</cols>'.
                '<sheetData>'
        );

        return $this;
    }

    public function initRow()
    {
        $this->lastCol = 0;
        $file = $this->getfile();
        $file->write('<row collapsed="false" customFormat="false" customHeight="false" hidden="false" ht="12.1" outlineLevel="0" r="'.($this->lastRow+1).'">');

        return $this;
    }


    /**
     * Row must be init with initRow() method before.
     * The row has to be close manualy after the last row element with closeRow() method.
     * Warning Row must be closed before a new one is open.
     *
     * @param string $v
     * @return lfm_XLSXWriter
     */
    public function addCell($v, $type = 'string')
    {
        $file = $this->getfile();
        $this->writeCell($file, $this->lastRow, $this->lastCol, $v, $type);
        $this->lastCol++;

        return $this;
    }


    public function closeRow()
    {
        $file = $this->getfile();
        $file->write('</row>');
        $this->lastRow++;

        return $this;
    }

    public function closeSheet()
    {
        $file = $this->getfile();
        $file->write(
            '</sheetData>'.
            '<printOptions headings="false" gridLines="false" gridLinesSet="true" horizontalCentered="false" verticalCentered="false"/>'.
            '<pageMargins left="0.5" right="0.5" top="1.0" bottom="1.0" header="0.5" footer="0.5"/>'.
            '<pageSetup blackAndWhite="false" cellComments="none" copies="1" draft="false" firstPageNumber="1" fitToHeight="1" fitToWidth="1" horizontalDpi="300" orientation="portrait" pageOrder="downThenOver" paperSize="1" scale="100" useFirstPageNumber="true" usePrinterDefaults="false" verticalDpi="300"/>'.
            '<headerFooter differentFirst="false" differentOddEven="false">'.
                '<oddHeader>&amp;C&amp;&quot;Times New Roman,Regular&quot;&amp;12&amp;A</oddHeader>'.
                '<oddFooter>&amp;C&amp;&quot;Times New Roman,Regular&quot;&amp;12Page &amp;P</oddFooter>'.
            '</headerFooter>'.
            '</worksheet>'
        );
        $file->close();
        $this->sheets_close = true;
    }


    public function writeSheet(array $data, $sheet_name='', array $header_types=array() )
    {
        parent::writeSheet($data, $sheet_name, $header_types=array() );
        $this->sheets_close = true;
    }




    public function downloadFile()
    {
        $this->sendHeaders();
        $this->writeToStdOut();
    }

    public function writeToFile($filename)
    {
        if(!$this->sheets_close){
            $this->closeSheet();
        }
        if($filename === null){
            //@todo Add path?
            $filename = $this->filename;
        }

        parent::writeToFile($filename);
    }


	/**
	 * Set correct headers to download as excel file
	 * @param	string	$filename
	 */
	private function sendHeaders() {

      	header("Pragma: public");
      	header("Expires: 0");
      	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
     	header("Content-Type: application/force-download");
      	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      	header('Content-Disposition: attachment;filename="'.$this->filename.'"');
      	header("Content-Transfer-Encoding: binary ");
	}



	/**
	 * Set the output method to download, the excel file will be sent to stdout
	 * and set the filename used for the download
	 * @param	string	$filename
	 * @return lfm_XLSXWriter
	 */
	public function setFilename($filename) {
	    $filename = str_replace(array('/','\\', ';', "'", '"'), array('-', '-', '', '', ''), $filename);
	    $this->filename = $filename;
	    return $this;
	}




	public function writeSheetHeader(array $header_row, array $data, $sheet_name='')
	{
        $this->sheets_init = true;
	    $sheet_filename = $this->tempFilename();
	    $sheet_default = 'Sheet'.(count($this->sheets)+1);
	    $sheet_name = !empty($sheet_name) ? $sheet_name : $sheet_default;
	    $this->sheets[] = array('filename'=>$sheet_filename, 'sheetname'=>$sheet_name ,'xmlname'=>strtolower($sheet_default).".xml" );

	    $row_count = count($data) + 1;
	    $column_count=0;
	    foreach($header_row as $v)
	    {
	        if(!is_array($v)){
	            $column_count++;
	        }else{
	            foreach($v as $vh){
	                $column_count++;
	            }
	        }
	    }
	    $max_cell = self::xlsCell( $row_count-1, $column_count-1 );
	    bab_debug($max_cell);

	    $tabselected = count($this->sheets)==1 ? 'true' : 'false';//only first sheet is selected

	    $file = new XLSXWriter_BuffererWriter($sheet_filename);
	    $file->write('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'."\n");
	    $file->write('<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">');
	    $file->write(    '<sheetPr filterMode="false">');
	    $file->write(        '<pageSetUpPr fitToPage="false"/>');
	    $file->write(    '</sheetPr>');
	    $file->write(    '<dimension ref="A1:'.$max_cell.'"/>');
	    $file->write(    '<sheetViews>');
	    $file->write(        '<sheetView colorId="64" defaultGridColor="true" rightToLeft="false" showFormulas="false" showGridLines="true" showOutlineSymbols="true" showRowColHeaders="true" showZeros="true" tabSelected="'.$tabselected.'" topLeftCell="A1" view="normal" windowProtection="false" workbookViewId="0" zoomScale="100" zoomScaleNormal="100" zoomScalePageLayoutView="100">');
	    $file->write(            '<selection activeCell="A1" activeCellId="0" pane="topLeft" sqref="A1"/>');
	    $file->write(        '</sheetView>');
	    $file->write(    '</sheetViews>');
	    $file->write(    '<cols>');
	    $file->write(        '<col collapsed="false" hidden="false" max="1025" min="1" style="0" width="11.5"/>');
	    $file->write(    '</cols>');
	    $file->write(    '<sheetData>');

        $file->write('<row collapsed="false" customFormat="false" customHeight="false" hidden="false" ht="12.1" outlineLevel="0" r="'.(1).'">');

        $k=0;
        foreach($header_row as $v)
        {
            if(!is_array($v)){
                $this->writeCell($file, 0, $k, $v, 'string');
                $k++;
            }else{
                foreach($v as $vh){
                     $this->writeCell($file, 0, $k, $vh, 'string');
                     $k++;
                }
            }
        }
        $file->write('</row>');

	    foreach($data as $i=>$row)
	    {
	        $file->write('<row collapsed="false" customFormat="false" customHeight="false" hidden="false" ht="12.1" outlineLevel="0" r="'.($i+1+1).'">');
	        $k=0;

	        foreach ($header_row as $h => $v) {
	            if(isset($row[$h])){
	                if(is_array($v)){
	                    foreach($v as $h2 => $v2){
	                        if(isset($row[$h][$h2])){
	                           $this->writeCell($file, $i+1, $k, $row[$h][$h2], 'string');
	                        }else{
	                           $this->writeCell($file, $i+1, $k, '', 'string');
	                        }
	                        $k++;
	                    }
	                }else{
	                    $this->writeCell($file, $i+1, $k, $row[$h], 'string');
	                    $k++;
	                }
	            }else{
	                if(is_array($v) && empty($v)){
	                    //DO NOTHING EMPTY COLOUMN
	                }else{
	                    $this->writeCell($file, $i+1, $k, '', 'string');
	                    $k++;
	                }
	            }
	        }
	        $file->write('</row>');
	    }
	    $file->write(    '</sheetData>');
	    $file->write(    '<printOptions headings="false" gridLines="false" gridLinesSet="true" horizontalCentered="false" verticalCentered="false"/>');
	    $file->write(    '<pageMargins left="0.5" right="0.5" top="1.0" bottom="1.0" header="0.5" footer="0.5"/>');
	    $file->write(    '<pageSetup blackAndWhite="false" cellComments="none" copies="1" draft="false" firstPageNumber="1" fitToHeight="1" fitToWidth="1" horizontalDpi="300" orientation="portrait" pageOrder="downThenOver" paperSize="1" scale="100" useFirstPageNumber="true" usePrinterDefaults="false" verticalDpi="300"/>');
	    $file->write(    '<headerFooter differentFirst="false" differentOddEven="false">');
	    $file->write(        '<oddHeader>&amp;C&amp;&quot;Times New Roman,Regular&quot;&amp;12&amp;A</oddHeader>');
	    $file->write(        '<oddFooter>&amp;C&amp;&quot;Times New Roman,Regular&quot;&amp;12Page &amp;P</oddFooter>');
	    $file->write(    '</headerFooter>');
	    $file->write('</worksheet>');
	    $file->close();

	    $this->sheets_close = true;
	}
}