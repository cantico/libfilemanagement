<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

function lfm_preinstall_str($id) {

	static $strings = array(
	
		'en' => array(
			0 => 'ImageMagick (thumbnails for pdf documents)',
			1 => 'FFmpeg as php extension or binary executable (thumbnails for videos)',
			2 => 'Available',
			3 => 'Not available',
			4 => 'Pdfinfo (metadata for pdf documents)',
			5 => 'id3 php module (metadata for mp3 audio files)',
			6 => 'exif php module (metadata for jpeg images)'
		),
	
		'fr' => array(
			0 => 'ImageMagick (miniatures pour les documents pdf)',
			1 => 'FFmpeg en extension php ou en binaire exécutable (miniatures pour les videos)',
			2 => 'Disponible',
			3 => 'Non disponible',
			4 => 'Pdfinfo (métadonnés pour les documents pdf)',
			5 => 'module php id3 (métadonnés des fichiers mp3)',
			6 => 'module php exif (métadonnés des images jpeg)'
		)
	);
	
	
	$lang = isset($GLOBALS['babLanguage']) ? $GLOBALS['babLanguage'] : 'en';
	
	if (!isset($strings[$lang])) {
	    $lang = 'en';
	}
	

	if (!function_exists(('bab_getStringAccordingToDataBase'))) {
		return $strings[$lang][$id];
	}
	
	
	
	return bab_getStringAccordingToDataBase($strings[$lang][$id], 'UTF-8');
}


function lfm_preinstall_execCmd($cmd) {
	
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibFileManagement/');
	$system_access = $registry->getValue('system_access', true);
	
	if (false === $system_access)
	{
		return false;
	}

	$handle = @popen($cmd, 'r');
	if ($handle) {
		$buffer = '';
		while(!feof($handle)) {
		   $buffer .= fgets($handle, 1024);
		}
		pclose($handle);
		return $buffer;
	} 
	
	return false;
}




function lfm_preinstall_imagemagick_version() {
	
	if (extension_loaded('imagick'))
	{
		$im = new Imagick;
		$arr = $im->getVersion();
		if (preg_match('/[\d\.\-]+/', $arr['versionString'], $m))
		{
			return $m[0];
		}
		return false;
	}
	
	
	static $version = NULL;
	if (NULL === $version) {
		$version = 0;
		if ($output = lfm_preinstall_execCmd('convert -version')) {
			if (preg_match('/Version:\sImageMagick\s([\d\.]+)/', $output, $matches)) {
				$version = $matches[1];
			}
		}
	}
	return $version;
}





function lfm_preinstall_pdfinfo_version() {
	static $version = NULL;
	if (NULL === $version) {
		$version = 0;
		if ($output = lfm_preinstall_execCmd('pdfinfo -v 2>&1')) {
			if (preg_match('/pdfinfo\s+version\s+([\d\.]+)/', $output, $matches)) {
				$version = $matches[1];
			}
		}
	}
	return $version;
}



function lfm_preinstall_ffmpeg()
{
	if (extension_loaded('ffmpeg'))
	{
		return true;
	}
	
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/LibFileManagement/');
	$system_access = $registry->getValue('system_access', true);
	
	if (false === $system_access)
	{
		return false;
	}
	
	
	@system('ffmpeg -version > /dev/null 2>&1', $ffmpeg_returnvalue);
	return (0 === (int) $ffmpeg_returnvalue);
}






		
		
return array(
	0 => array(
	
		'description' 		=> lfm_preinstall_str(0),
		'required' 			=> false,
		'recommended' 		=> '6.2.5',
		'current'			=> lfm_preinstall_imagemagick_version(),
		'result'			=> version_compare('6.2.5', lfm_preinstall_imagemagick_version(), '<=')
	
	),
	
	1 => array(
	
		'description' 		=> lfm_preinstall_str(1),
		'required' 			=> false,
		'recommended' 		=> lfm_preinstall_str(2),
		'current'			=> lfm_preinstall_ffmpeg() ? lfm_preinstall_str(2) : lfm_preinstall_str(3),
		'result'			=> lfm_preinstall_ffmpeg()

	),
	
	2 => array(
	
		'description' 		=> lfm_preinstall_str(4),
		'required' 			=> false,
		'recommended' 		=> '0.1',
		'current'			=> lfm_preinstall_pdfinfo_version(),
		'result'			=> version_compare('0.1', lfm_preinstall_pdfinfo_version(), '<=')

	),
	
	3 => array(
	
		'description' 		=> lfm_preinstall_str(5),
		'required' 			=> false,
		'recommended' 		=> lfm_preinstall_str(2),
		'current'			=> extension_loaded('id3') ? lfm_preinstall_str(2) : lfm_preinstall_str(3),
		'result'			=> extension_loaded('id3')

	),
	
	4 => array(
	
		'description' 		=> lfm_preinstall_str(6),
		'required' 			=> false,
		'recommended' 		=> lfm_preinstall_str(2),
		'current'			=> extension_loaded('exif') ? lfm_preinstall_str(2) : lfm_preinstall_str(3),
		'result'			=> extension_loaded('exif')

	)
);

	


